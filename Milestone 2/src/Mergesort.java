public class Mergesort {
	
	public static int[] sort(int[] a, int l, int r ){
		if (l<r) {
			for (int k=l; k<=r;k++) {
				System.out.print(a[k] + "| ");
			}
			System.out.println();
			int q=(l+r)/2;
			sort(a,l,q);
			sort(a,q+1,r);
			merge (a,l,q,r);
			for (int k=l; k<=r;k++) {
				System.out.print(a[k] + "| ");
			}
			System.out.println();
		}
		return a;
	}
	
	private static void merge(int[] a, int l, int q, int r) {
		int b[] = new int[a.length];
		int i,j;
		for (i=l; i<=q;i++) {
			b[i]=a[i];
		}
		for (j=q+1; j<=r; j++) {
			b[r+q+1-j]=a[j];
		}
		i=l;
		j=r;
		for (int k=l; k<=r;k++) {
			if (b[i]<=b[j]) {
			a[k]=b[i];
			i++;
			}else {
				a[k]=b[j];
				j--;
			}
		}
		for (int i1 = 0; i1<a.length; i1++) {
			if (i1+1==a.length) {
			System.out.print(a[i1]);
			System.out.println();}
			else{System.out.print(a[i1] + ",");}
		}
	}

	public static void main(String[] args) {
		int[] a = {2,1,5,7,6,4,3,8};
		System.out.println("Mergesort->->->");
		System.out.println("------------------------------");
		sort(a, 0, a.length-1);
		System.out.println("------------------------------");
		System.out.println("Sortiertes Array:");
		for (int i=0; i<a.length; i++) {System.out.print(a[i] + "| ");}
	}
}