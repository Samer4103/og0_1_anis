package omnom;

public class HaustierJava {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	
	public HaustierJava(String name) {
		this.name = name;
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
	}
	
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		if (hunger < 101 && hunger > -1)
			this.hunger = hunger;
	}
	public int getMuede() {
		return muede;
	}
	public void setMuede(int muede) {
		if (muede < 101 && muede > -1)
			this.muede = muede;
	}
	public int getZufrieden() {
		return zufrieden;
	}
	public void setZufrieden(int zufrieden) {
		if (zufrieden < 101 && zufrieden > -1)
			this.zufrieden = zufrieden;
	}
	public int getGesund() {
		return gesund;
	}
	public void setGesund(int gesund) {
		if (gesund < 101 && gesund > -1)
			this.gesund = gesund;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	
	//Weiter Methoden
	public void fuettern(int wert) {
		if (!(wert + hunger > 100))
			hunger = hunger + wert;
		else
			hunger = 100;
		
	}

	public void schlafen(int wert) {
		if (!(wert + muede > 100))
			muede = muede + wert;
		else
			muede = 100;
	}

	public void spielen(int wert) {
		if (!(wert + zufrieden > 100))
			zufrieden = zufrieden + wert;
		else
			zufrieden = 100;
	}

	public void heilen() {
		gesund = 100;
		
	}
	
	
}
//
