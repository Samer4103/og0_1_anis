public class Laufzeitanalyse {

    public static int[] bubblesort(int a[]){
        int temp=0;
        int n=0;
        for (int i=1;i<a.length;i++) {
            for (int j=0; j<a.length-i;j++) {
                if (a[j]>a[j+1]) {
                    n++;
                    temp=a[j];
                    a[j]=a[j+1];
                    a[j+1]=temp;
                }
            }
        }
        System.out.print(n + " mal vertauscht. \n");
        return a;
    }
    
    public static void main(String[] args) {
    int bestcase[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100};
    System.out.print("Zahlen wurden in bestcase "); 
    Laufzeitanalyse.bubblesort(bestcase);
    int averagecase[] = {57, 82, 4, 68, 39, 69, 7, 22, 67, 6, 51, 94, 74, 50, 72, 37, 76, 70, 62, 31, 35, 58, 28, 55, 30, 88, 29, 20, 56, 12, 33, 80, 42, 60, 95, 48, 87, 44, 89, 34, 49, 26, 40, 2, 14, 23, 90, 99, 38, 9, 98, 18, 8, 77, 53, 10, 19, 84, 32, 1, 46, 24, 73, 83, 21, 5, 93, 43, 36, 71, 79, 25, 61, 45, 27, 17, 100, 65, 92, 78, 86, 85, 11, 13, 15, 97, 64, 3, 52, 96, 59, 66, 81, 63, 16, 47, 75, 41, 54, 91};
    System.out.print("Zahlen wurden in averagecase ");
    Laufzeitanalyse.bubblesort(averagecase);
    int worstcase[] = {100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    System.out.print("Zahlen wurden in worstcase ");
    Laufzeitanalyse.bubblesort(worstcase);
    }
}

