public class Laufzeitanalyse {
	
	public static int[] sort(int[] a, int l, int r){
		if (l<r) {
			int q=(l+r)/2;
			sort(a,l,q);
			sort(a,q+1,r);
			merge (a,l,q,r);
			System.out.print("|");
		}
		return a;
	}	
	private static void merge(int[] a, int l, int q, int r) {
		int b[] = new int[a.length];
		int i,j;
		for (i=l; i<=q;i++) {
			b[i]=a[i];
		}
		for (j=q+1; j<=r; j++) {
			b[r+q+1-j]=a[j];
		}
		i=l;
		j=r;
		for (int k=l; k<=r;k++) {
			if (b[i]<=b[j]) {
			a[k]=b[i];
			i++;
			}else {
				a[k]=b[j];
				j--;
			}
		}	
	}

	public static void main(String[] args) {
		int[] bestcase = {1,2,3,4,5,6,7,8}; 
		int[] averagecase = {2,1,5,7,6,4,3,8};
		int[] worstcase = {8,7,6,5,4,3,2,1};
		System.out.print("Bestcase->->-> \nZahlen wurden ");
		sort(bestcase, 0, bestcase.length-1);
		System.out.print(" mal in bestcase vertauscht.");
		System.out.println();
		System.out.print("Averagecase->->-> \nZahlen wurden ");
		sort(averagecase, 0, averagecase.length-1);
		System.out.print(" mal in Averagecase vertauscht.");
		System.out.println();
		System.out.print("Worstcase->->-> \nZahlen wurden ");
		sort(worstcase, 0, worstcase.length-1);
		System.out.print(" mal in worstcase vertauscht.");
		System.out.println();
	}
}